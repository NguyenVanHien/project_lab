<?php
class Base extends MY_Controller{

	function __construct(){
		parent::__construct();
		
		// $this->load->library('rest');
		// $config =  array('server' => base_url('api'));
		// $this->rest->initialize($config);
		$this->faker = Faker\Factory::create();
		$this->data = array('title'=> 'Base Chào mừng đến bài test giả lập',);
	}

	public function seed(){
    $this->_seed_users(100);
  }

  public function _seed_users($limit)
  {
    for ($i = 0; $i < $limit; $i++) {
      $data = array(
          'fullName' => $this->faker->firstName . ' ' . $this->faker->lastName,
          'gender' => rand(0, 1) ? 'nam' : 'nu',
          'birthday' => $this->faker->dateTimeThisCentury->format('Y-m-d H:i:s'),
          'createdDate' => strtotime(date('Y-m-d H:i:s')),
          'sdtk' => rand(10000, 20000)
      );
      $this->mongo_db->insert('test', $data);
    }
		return redirect()->to('/lab/base');
  }


	public function index(){
		// Why Order by query is not working?
		// $this->mongo_db->from($this->test);
		// $this->mongo_db->order_by("fullName", "desc");
		$data['data'] = $this->mongo_db->get('test');
		$this->load->view('base', $data);	
	}

	public function create(){
		$csrf = array(
      'csrfname' => $this->security->get_csrf_token_name(),
      'csrfhash' => $this->security->get_csrf_hash()
		);

		$this->load->view('create',$csrf);
	}

	public function store () {
		$data = $this->input->post();
		$data['createdDate'] = strtotime(date('Y-m-d H:i:s'));
		$this->mongo_db->insert('test', $data);
		return redirect()->to('/lab/base');
	}
}
?>