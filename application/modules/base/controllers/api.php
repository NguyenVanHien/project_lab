<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Api extends REST_Controller {
	function __construct(){
		parent::__construct();
		$this->r = array('status'=>200,'message'=>'error');
		
	}
	public function index_get(){
		$this->response($this->r);
	}
	public function index_post(){
		$this->response($this->r);
	}
	public function index_put(){
		$this->response($this->r);
	}
	public function index_del(){
		$this->response($this->r);
	}
}	

?>