<!DOCTYPE html>
<html>
<head>
	<title>List info</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<script
  src="https://code.jquery.com/jquery-3.5.0.min.js"
  integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
  crossorigin="anonymous"></script>

</head>
<body>
	<div class="container">
		<div class='my-4'>
			<a href='/lab/base/create' class="btn btn-info">Create</a>
		</div>
		<div>
			<table id="myTable" class="table dataTable ">
				<thead>
					<tr>
						<th>ID</th>
						<th>Họ và tên</th>
						<th>Ngày sinh</th>
						<th>Gioi tinh</th>
						<th>SDTK</th>
						<th>Ngày thành lập</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($data as $item): ?>
					<tr>
						<td><?= $item['_id'] ?></td>
						<td><?= $item['fullName'] ?></td>
						<td><?= $item['birthday'] ?></td>
						<td><?= $item['gender']?></td>
						<td><?= $item['sdtk'] ?></td>
						<td><?= date('d/m/Y', $item['createdDate']) ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
	
	<script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#myTable').DataTable();
		} );
	</script>
</body>
</html>