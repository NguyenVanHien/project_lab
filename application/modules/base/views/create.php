<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script
  src="https://code.jquery.com/jquery-3.5.0.min.js"
  integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
  crossorigin="anonymous"></script>

</head>
<body>
	<div class="container">
		<form method="post" class="mt-5" action='/lab/base/store'>
			<input type="hidden" name="<?=$csrfname?>" value="<?= $csrfhash ?>" />
		  <div class="form-group row">
		    <label for="fullName" class="col-sm-2 col-form-label">Họ và tên</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" name="fullName" id="fullName" required="required">
		    </div>
		  </div>
		  <div class="form-group row">
		    <label for="birthday" class="col-sm-2 col-form-label">Ngày sinh</label>
		    <div class="col-sm-10">
		      <input type="date" class="form-control" name="birthday" id="birthday" required="required">
		    </div>
		  </div>
		  <div class="form-group row">
		  	 	<label for="gender" class="col-sm-2 col-form-label">Gioi tính</label>
		  		<div class=" col-sm-10">
		  			<div class="form-check">
		  				 <input class="form-check-input" type="radio" name="gender" id="nam" value="nam" required="required" checked>
					  	<label class="form-check-label" for="nam">Nam</label>
		  			</div>
					 	<div class="form-check">
					 		<input class="form-check-input" type="radio" name="gender" id="nu" value="nu" required="required">
					  	<label class="form-check-label" for="nu">Nữ</label>
					 	</div>
					</div>
		  </div>
		 
			<div class="form-group row">
		    <label for="sdtk" class="col-sm-2 col-form-label">SDTK</label>
		    <div class="col-sm-10">
		      <input type="number" min="10000" class="form-control" name="sdtk" id="sdtk" required="required">
		    </div>
		  </div>
		  <div class="form-group row">
		    <div class="col-sm-10 offset-sm-2">
		      <input type="submit" class="btn btn-primary" name="" id="submit">
		    </div>
		  </div>
		</form>
	</div>	
</body>
</html>