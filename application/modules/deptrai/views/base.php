<!DOCTYPE html>
<html>
<head>
	<title>List info</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<script
  src="https://code.jquery.com/jquery-3.5.0.min.js"
  integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ="
  crossorigin="anonymous"></script>

</head>
<body>
	<div class="container">
		<div class='my-4'>
			<a href='/lab/deptrai/create' class="btn btn-info">Create</a>
		</div>
		<div>
			<table id="myTable" class="table dataTable ">
				<thead>
					<tr>
						<th>ID</th>
						<th>Họ và tên</th>
						<th>Ngày sinh</th>
						<th>Gioi tinh</th>
						<th>SDTK</th>
						<th>Ngày thành lập</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	
	<script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
		$(document).ready( function () {
		    $('#myTable').DataTable();

			function getFormattedDate(timestamp) {
				var date = new Date(parseInt(timestamp) * 1000);

				var month = date.getMonth() + 1;
				var day = date.getDate();
				var hour = date.getHours();
				var min = date.getMinutes();
				var sec = date.getSeconds();

				month = (month < 10 ? "0" : "") + month;
				day = (day < 10 ? "0" : "") + day;
				hour = (hour < 10 ? "0" : "") + hour;
				min = (min < 10 ? "0" : "") + min;
				sec = (sec < 10 ? "0" : "") + sec;

				var str = day + '-' + month + '-' + date.getFullYear();

				/*alert(str);*/

				return str;
			}

			$.ajax({
				url: '/lab/deptrai/api',
				type: 'GET',
				// beforeSend: function () {
				// 	alert(1);
				// },
				dataType: 'json'
			}).done(function (result) {
				let data = result['data'];
				let html = '';

				data.map((value, index) => {
					html += `
						<tr>
							<td>${value._id.$id}</td>
							<td>${value.fullName}</td>
							<td>${value.birthday}</td>
							<td>${value.gender}</td>
							<td>${value.sdtk}</td>
							<td>${getFormattedDate(value.createdDate)}</td>
						</tr>
					`;
				});

				$('tbody').html(html);
				console.log(data);
			}).fail(function (error) {
				console.log(error);
			});
		} );
	</script>
</body>
</html>