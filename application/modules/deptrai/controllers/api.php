<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Api extends REST_Controller {
	function __construct(){
    parent::__construct();
		$this->load->model('deptrai_model');
		$this->r = array('status'=>200,'message'=>'success','data'=>'');
		
	}
	public function index_get(){
		$this->r['data'] = $this->deptrai_model->list_data(0,100);
		$this->response($this->r);
	}
	public function index_post(){
		$data = json_decode(file_get_contents('php://input'), true); // Lấy tất cả giá trị input
		$data['createdDate'] = strtotime(date('Y-m-d H:i:s'));
		$result = $this->deptrai_model->insert_data($data);
		$this->r['data'] = $result;
		
		$this->response($this->r);
	}
	public function index_put(){
		$this->response($this->r);
	}
	public function index_del(){
		$this->response($this->r);
	}
}	

?>